package main

import (
	"github.com/dgrijalva/jwt-go"
	"crypto/rsa"
	"fmt"
	"strings"
	"io/ioutil"
	"encoding/json"
	"time"
)

type claim struct {
	Exp      int    `json:"exp"`
	Id       string `json:"id"`
	Iss      string `json:"iss"`
	OrigIat  int    `json:"orig_iat"`
	UserId   string `json:"user_id"`
	UserType string `json:"user_type"`
	Email    string `json:"email"`
}

func main() {
	//Token示例
	tokenString := "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNvbnN1bWVyQGsyZGF0YS5jb20uY24iLCJleHAiOjE1MjQ4ODUwNDIsImlkIjoiY29uc3VtZXJAazJkYXRhLmNvbS5jbiIsImlzcyI6ImlhbSIsIm9yaWdfaWF0IjoxNTI0ODg0NDQyLCJ1c2VyX2lkIjoidXNyLTY5NjE2ZGEwOTZmZDgxZWJiZWM1OTRhZGFiNTM3MWFhNzFmNzY3IiwidXNlcl90eXBlIjoiY29uc3VtZXIifQ.V4b-oN14Lm_gnrE_bccLVmUq6hqfbSocd7bQfw30mhvy1cSsA3M0-12s3MualRuBD42_MJLSyPb_StUGlp-hjw"

	var err error

	//加载pub.key公钥文件
	key, err := ioutil.ReadFile("./pub.key")
	if err != nil {
		panic(err)
	}

	//创建公钥
	var rsaPSSKey *rsa.PublicKey
	if rsaPSSKey, err = jwt.ParseRSAPublicKeyFromPEM(key); err != nil {
		panic(err)
	}

	parts := strings.Split(tokenString, ".")
	method := jwt.GetSigningMethod("RS256")

	//Token 使用pub.key进行RS256算法验证
	err = method.Verify(strings.Join(parts[0:2], "."), parts[2], rsaPSSKey)
	if err != nil {
		panic(err)
	}
	fmt.Println("alg verify success")

	//解析Payload
	payload, err := jwt.DecodeSegment(parts[1])
	var c claim
	json.Unmarshal(payload, &c)
	fmt.Println("orig_iat: ", c.OrigIat)
	fmt.Println("exp:      ", c.Exp)
	fmt.Println("id:       ", c.Id)
	fmt.Println("iss:      ", c.Iss)
	fmt.Println("user_id:  ", c.UserId)
	fmt.Println("user_type:", c.UserType)
	fmt.Println("email:    ", c.Email)

	fmt.Println("是否过期:  ", time.Now().Second()-c.Exp < 0)
}
